#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "parse_qtree.h"

node_t* parseFile(char* filePath) {
  
  FILE* file = NULL;
  FILE* logFile = NULL;
  char buff[BUFF_SIZE];
  char c;
  
  int buffPointer = 0;
  int lineCount = 1;          
  int brackerCount = 0;       
  int closeBrackerCount = 0;
  int braceCount = 0;
  int closeBraceCount = 0;
  int classCount = 0;
  int mode = READ;       // tryb czytania
  int linePointer = 0;        
  int cCount = 0;
  int isError = 0;
  int currentLineCCount = 0; 
  node_t* node = NULL;        // Galaz na ktorej buduje drzewo
  node_t* root = NULL;
  
  root = createNode();
  if (root == NULL) {
    printf("Brak pamięci na alokację korzenia drzewa\n");
    return NULL;
  }
  setCode(root, "ROOT");
  node = root;
  printf("+===========================================+\n|\n");
  printf("| * Start open file: %s", filePath); 
  
  if (!filePath) {
    printf("| * FilePath is null, start parse stdin");
    file = stdin; 
  } else {
    file = fopen(filePath, "r");
  }

  if (!file) {
    printf("| * Error when opening file. Exit program\n");
    printf("+==========================================+\n");
    return NULL;
  }

  logFile = fopen("logFile.log", "a");

  fprintf(logFile, "############## %s #########################\n", filePath);
  printf("| * start reading file:\n");
  printf("| %d: ", lineCount);
  currentLineCCount = 0;
  while (isError == 0 && !feof(file) && (c = fgetc(file)) != EOF ) {
    cCount++;
    if (c != '\n') {
      printf("%c", c);
      currentLineCCount++;
    }
    else { 
      printf("\n| %d: ", lineCount++);
      currentLineCCount = 0;
    }
    switch (mode) {
      case READ:
        switch (c) {
          case '[':
            brackerCount++;
            fprintf(logFile, "Przeczytano [\n");
            if (!feof(file) && (c = fgetc(file)) == '.') { 
              mode = NODE;
              node_t* tmp;
              if ((tmp = createNode()) == NULL) {
                isError = ERROR_NODE_NULL;
                printf("Brak pamięci\n");
              } else {
                if (node != NULL) {
                  pinLinkedListNode(node, tmp);
                } else {
                  root = tmp;
                }
                node = tmp;
              }
            }
            else {
              fprintf(logFile, "po [ nie znaleziono kropki. Przerywam działanie\n");
              isError = PARSE_NODE_ERROR;
            }
            printf("%c", c);
            cCount++;
            currentLineCCount++;
            break;
          case '{':
            braceCount++;
            fprintf(logFile, "Przeczytano {, zaczynam czytać wartość obiektu\n");
            if (node && node->value)
              isError = PARSE_CODE_ERROR;
            else
              mode = OBJECT;
            break;
          case ']':
            closeBrackerCount++;
            fprintf(logFile, "przechodze do parent\n");
            node = node->parent;
            if (!node)
              isError = SYNTAX_ERROR;
            break;
        }
        break;
      case NODE:
        switch (c) {
          case ']':
            closeBrackerCount++;
            fprintf(logFile, "Przeczytano znak ], przechodzę do zapisania node\n");
            buff[buffPointer] = '\0';
            buffPointer = 0;
            if (node == NULL) {
              fprintf(logFile,"error node is null");
            }
            setCode(node, buff);

            node = node->parent;
            mode = READ;
            break;
          case '{':
            braceCount++;
            fprintf(logFile, "Przeczytano znak {, przechodzę do zapisania node a następnie czytam obiekt\n");
            buff[buffPointer] = '\0';
            buffPointer = 0;
            if (node == NULL) {
              fprintf(logFile,"error node is null");
            }
            fprintf(logFile, "Kod to %s\n", buff);
            setCode(node, buff);
            mode = OBJECT;
            break;
          case '[':
            brackerCount++;
            fprintf(logFile, "Przeczytano [\n");
            buff[buffPointer] = '\0';
            buffPointer = 0;
            if (node == NULL) {
              fprintf(logFile,"error node is null");
            }
            setCode(node, buff);
            if (!feof(file) && (c = fgetc(file)) == '.') { 
              mode = NODE;
              node_t* tmp;
              if ((tmp = createNode()) == NULL) {
                isError = ERROR_NODE_NULL;
                printf("Brak pamięci\n");
              } else {
                if (node != NULL) {
                  pinLinkedListNode(node, tmp);
                } else {
                  root = tmp;
                }
                node = tmp;
              }
            }
            else { 
              fprintf(logFile, "po [ nie znaleziono kropki. Przerywam działanie\n");
              isError = PARSE_NODE_ERROR;
            } 
            
            break;
          case '}':
            fprintf(logFile, "Error: mode = NODE find }\n");
            isError = PARSE_CODE_ERROR;
            break;
          case '\n':
            //nic nie rob, pomin znak
            break;
          default:
            if (buffPointer == BUFF_SIZE)
              isError = BUFF_OVERFLOW;
            else
              buff[buffPointer++] = c;
            break;
        }
        break;
      case OBJECT:
        switch(c) {
          case '[':
          case '{':
          case ']':
          case '.':
            fprintf(logFile, "Error: syntax error - unexpected sign\n");
            isError = PARSE_VALUE_ERROR;
            break;
          case '}':
            closeBraceCount++;
            fprintf(logFile, "Przeczytałem }. Rozpooczynam tworzenie obiektu i dodannia go do aktualnego node\n");
            buff[buffPointer] = '\0';
            buffPointer = 0;
            object_t* object = NULL;
            if ((object = createObject(buff)) == NULL) {
                isError = ERROR_NODE_NULL;
                printf("Brak pamięci\n");
            } else {
              fprintf(logFile, "Wartość wynosi: %s\n", buff);
              if (node == NULL) {
                fprintf(logFile, "Błąd: node jest rowne NULL\n");
                isError = ERROR_NODE_NULL;
              } else {
                node->value = object;
              }
              mode = READ;
            }
            break;
          case '\n':
            // pomin nic nie rob
            break;
          default:
            if (buffPointer == BUFF_SIZE)
              isError = BUFF_OVERFLOW;
            else
              buff[buffPointer++] = c;
            break;
        }
        break;
    }
  }
  if (isError == 0)
    printf("\n| * I've finished\n");
  else {
    // w przypadku bledu wypisz znak ktory jest w buforze
    while ((c = fgetc(file)) != EOF && !feof(file) && c != '\n')
      fputc(c, stdout);
    printf("\n| *:");
    while (currentLineCCount-- > 0) {
      printf(" ");
    }
    printf("^\n");
    printf("| * Error while parsing\n");
    printf("| * ErroCode: %d\n", isError);
    freeLinkedNodeRecursive(root);
    root = NULL;
  }
  printf("+============================================+\n");
  fprintf(logFile, "\n================ Summary ========================\n");
  fprintf(logFile, "1. Readed line %d\n", lineCount);
  fprintf(logFile, "2. Readed character %d\n", cCount);
  fprintf(logFile, "3. Readed Braces %d and close braces %d\n", braceCount, closeBraceCount);
  fprintf(logFile, "4. Readed brackets %d and close brackets %d\n", brackerCount, closeBrackerCount);
  fclose(file);
  fclose(logFile);

  return root;
}



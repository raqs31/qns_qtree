#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "object.h"

object_t* createObject(char* value) {
  object_t* object = NULL;
  int size = OBJECT_SIZE;
  int lenght = 0;

  object = malloc(OBJECT_SIZE * sizeof *object);

  if (!object) {
    perror("createObject fail: return NULL\n");
    return NULL;
  } 

  if (value != NULL) {
    lenght = strlen(value);

    while (lenght > size)
      size *= 2;
  }

  object->value = malloc(size * sizeof(char));

  if (!object->value) {
    perror("createObject fail: not enought memmory to allocate value\n");
    free(object);
    return NULL;
  }

  object->lenght = lenght;
  object->size = size;
  
  if (value != NULL)
    strcpy(object->value, value);

  return object;
}

void freeObject(object_t** objectAddr) {
  if(!objectAddr)
    return;
  if (!*objectAddr)
    return;
  
  object_t* object = *objectAddr;

  if (object->value)
    free(object->value);

  free(object);
}

int compareObject(object_t* cmp1, object_t* cmp2) {
  if (!cmp1 && !cmp2) 
    return 0;
  else if (!cmp1 && cmp2)
    return -1;
  else if (cmp1 && !cmp2)
    return 1;
  else if ((cmp1->size > 0 || cmp2->size > 0) && cmp1->size != cmp2->size) 
    return cmp1->size - cmp2->size;
  else if (cmp1->size > 0 && cmp2->size > 0)
    return strcmp(cmp1->value, cmp2->value);
  else
    return 0;
}

void printObject(object_t* object) {
  if(!object) {
    return;
  }

  printf("**********************************************************************\n");
  printf("+object lenght %d\n", object->lenght);
  printf("+object size   %d\n", object->size);
  printf("+object value \"%s\"\n", object->value);
  printf("**********************************************************************\n");
}

#define OBJECT_SIZE 16

typedef struct object {
  int lenght;
  int size;
  char* value;
} object_t;

object_t* createObject(char* value);

void printObject(object_t* object);

void freeObject(object_t** object);

int compareObject(object_t* cmp1, object_t* cmp2);

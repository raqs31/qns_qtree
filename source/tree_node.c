#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tree_node.h"

static int keyCount = 0;

// funkcja tworzaca node
node_t* createNode(char* string) {
  node_t* node = NULL;
  node = malloc(sizeof *node);

  if (!node) {
    perror("createNode fail: return NULL\n");
    return NULL;
  }
  node->key = keyCount++;
  node->size = SIZE;
  node->capacity = 0;
  node->linkedListCapacity = 0;
  node->value = NULL;
  node->level = 0;
  node->parent = NULL;
  node->lastChild = NULL;
  node->firstChild = NULL;
  node->next = NULL;
  node->prev = NULL;
  node->children = malloc(node->size * sizeof *node->children);
  node->code[0] = '\0';
  return node;
  }

void setCode(node_t* node, char* string) {
  if (!string) {
    strcpy(node->code, "MOCK\0");
  } else {
    int lenght = strlen(string);
    if (lenght > CODE_LENGHT)
      lenght = CODE_LENGHT;
    strncpy(node->code, string, lenght);
      node->code[CODE_LENGHT] = '\0';
  }
}


int pinNode(node_t* parent, node_t* child) {
  if (!parent || !child)
    return PIN_FAIL_NULL_VALUE;
  
  if (parent->size == parent->capacity) {
    node_t** tmpChildren = parent->children;
    int newSize = parent->size * MULTIPLIER;
    
    parent->children = realloc(parent->children, newSize * sizeof *parent->children);
    
    if (!parent->children) {
      perror("pinNode fail: realloc fail");
      parent->children = tmpChildren;
      return PIN_FAIL_ALLOC;
    }
    parent->size = newSize;
  }

  parent->children[parent->capacity++] = child;
  child->parent = parent;
  resetLevelRecursive(child, parent->level + 1);
  return PIN_SUCCES;
}

int pinLinkedListNode(node_t* parent, node_t* child) {
  if (!parent || !child)
    return PIN_FAIL_NULL_VALUE;
  else if (!parent->firstChild && !parent->lastChild) {
    parent->firstChild = child;
    parent->lastChild = child;
    parent->linkedListCapacity = 1;
    child->parent = parent;
    child->level = parent->level + 1;
    return PIN_SUCCES;
  }
  else if (parent->firstChild) {
    node_t* parentChild  = parent->firstChild;

    while (parentChild->next)
      parentChild = parentChild->next;
    parentChild->next = child;
    child->parent = parent;
    child->level = parent->level + 1;
    parent->lastChild = child;
    parent->linkedListCapacity++;
    return PIN_SUCCES;
  }
}

node_t* unpinNodeFromParent(node_t* node) {
  if (!node)
    return NULL;
  if (!node->parent)
    return node;
  
  node_t* parent = node->parent;

  int i = 0;
  int j = 0;
  for (i, j; i < parent->capacity && j < parent->capacity; i++, j++) {
    if (parent->children[i] == node)  
      parent->children[i] = parent->children[++j];
    else 
      parent->children[i] = parent->children[j];
  }
  while (i < j)
    parent->children[i] = NULL;
  parent->capacity -= 1;
  resetLevelRecursive(node, 0);
  return node;
}

int compareNode(node_t* cmp1, node_t* cmp2) {
  if (!cmp1 && !cmp2)
    return 0;
  else if (!cmp1 && cmp2) 
    return -1;
  else if (cmp1 && !cmp2)
    return 1;
  else 
    return compareObject(cmp1->value, cmp2->value);
}

node_t* unpinLinkedNode(node_t* node) {
  if (node && (node->prev || node->next)) {
    node_t* prev = node->prev;
    node_t* next = node->next;

    if (prev)
      prev->next = next;
    if (next)
      next->prev = prev;

    node->parent->linkedListCapacity--;

  }
  return node;
  
}

void resetLevelRecursive(node_t* node, int startLevel) {
  node->level = startLevel;
  int i = 0;

  for (i; i < node->capacity; i++) {
    resetLevelRecursive(node->children[i], startLevel + 1);
  }
}

void resetLevelLinkedListRecursive(node_t* node, int startLevel) {
  node->level = startLevel;
  node_t* child = node->firstChild;

  while (child) {
    resetLevelLinkedListRecursive(child, startLevel + 1);
    child = child->next;
  }

}

// funcka zwalniajaca pamiec rekursywnie poczawszy od zadanego node
void freeNodeRecursive(node_t** nodeAddr) {
  if (!nodeAddr)
    return;
  if (!*nodeAddr)
    return;
  node_t* node = *nodeAddr;
  if (node->capacity > 0) {
    int i = 0;
    for (i; i < node->capacity; i++) {
      freeNodeRecursive(&node->children[i]);
    }
  }
  freeObject(&node->value);
  if (node->children)
    free(node->children);
  free(node);
  *nodeAddr = NULL;
}

void freeLinkedNodeRecursive(node_t* node) {
  if (!node)
    return;
  while(node->firstChild) {
    node_t* tmp = node->firstChild;
    node->firstChild = tmp->next;
    freeLinkedNodeRecursive(tmp);
    node->linkedListCapacity--;
  }
  freeObject(&node->value);
  // dla starego zalozenia
  if (node->children) {
    
    free(node->children);
  }
  free(node);
}
// funkcja znajdujaca korzen drzewa
node_t* findRoot(node_t* current) {
  if (!current)
    return NULL;  
  if (!current->parent) {//jesli nie ma parenta sam jest parentem
    return current;
  }

  node_t* node = current;
  while (node->parent) 
    node = node->parent;

  return node;
}
// funkcja pobierajaca przechowywana wartosc node
char* getValue(node_t* node) {
  if (!node)
    return NULL;
  if (!node->value)
    return NULL;
  return node->value->value;
}
// funkcja do wypisania wlasnosci node
void printNode(node_t* node) {
  if (!node) {
    return;
  }
  printf("+----------------------------------------------------------\n");
  printf("+node key      %d\n", node->key);
  printf("+node size     %d\n", node->size);
  printf("+node children %d\n", node->capacity);
  printf("+node level    %d\n", node->level);
  if (node->parent != NULL)
    printf("+node have parent\n");
  else
    printf("+node haven't parent\n");

  printf("+node object:\n");
  printObject(node->value);
  printf("+-----------------------------------------------------------\n");
}

void printLinkedNode(node_t* node) {
  if (!node) {
    printf("node is null\n");
    return;
  }
  printf("+----------------------------------------------------------\n");
  printf("+node key      %d\n", node->key);
  printf("+node children %d\n", node->linkedListCapacity);
  printf("+node level    %d\n", node->level);
  printf("+code          %s\n", node->code);
  if (node->parent != NULL)
    printf("+node have parent\n");
  else
    printf("+node haven't parent\n");

  printf("+node object:\n");
  printObject(node->value);
  printf("+-----------------------------------------------------------\n");
}

void printShortNode(node_t* node) {
  if (!node) 
    printf("[]");
  else {
    int i;
    for (i = 0; i < node->level; i++) {
      fputc('\t', stdout);
    }
    printf("[K:%d L:%d C:%s V:%s]\n", node->key, node->level, node->code, node->value == NULL ? "BRAK": node->value->value);
  }
}

void printTree(node_t* node) {
  if(!node)
    return;
  else {
    printShortNode(node);
    node_t* tmp = node->firstChild;
    while (tmp) {
      printTree(tmp);
      tmp = tmp->next;
    }
  }
}

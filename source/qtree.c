#include <stdio.h>

#include "parse_qtree.h"

void main(int argc, char** argv) {

  char* fileName = NULL;

  if (argc > 1) {
    fileName = argv[1];
  }
  
  node_t* root = parseFile(fileName);

  if (root == NULL) {
    printf("Utworzenie drzewa nie powiodło się\n");
    if (fileName != NULL) {
      printf("Plik %s nie istnieje bądź ma błędny format\n", fileName);
    } else {
      printf("Dane pobrane ze strumienia wejściowego są puste lub niepoprawne\n");
    }
  } else {
    printTree(root);
    freeLinkedNodeRecursive(root);
  }

}

#include "tree_node.h"

#define BUFF_SIZE 128
// read mode
#define NODE 1
#define OBJECT 2
#define READ 0
// errors dictionary
#define PARSE_VALUE_ERROR 1
#define PARSE_CODE_ERROR 2
#define PARSE_NODE_ERROR 3
#define PARSE_OBJECT_ERROR 4
#define ERROR_NODE_NULL -1
#define BUFF_OVERFLOW -10
#define SYNTAX_ERROR -5

node_t* parseFile(char* filePath);

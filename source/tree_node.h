#include "object.h"
#define SIZE 2
#define MULTIPLIER 2
#define PIN_SUCCES 0
#define PIN_FAIL_ALLOC -1
#define PIN_FAIL_NULL_VALUE -2
#define CODE_LENGHT 4
typedef struct tree_node {
  int key;
  int capacity;
  int linkedListCapacity;
  int size;
  int level;
  char code[CODE_LENGHT + 1];
  struct object* value;
  struct tree_node* parent;
  struct tree_node** children;
  struct tree_node* lastChild;

  struct tree_node* firstChild;
  struct tree_node* next;
  struct tree_node* prev;
} node_t;

node_t* findRoot(node_t* current);

char* getValue(node_t* node);

node_t* createNode();

void setCode(node_t* node, char *string);

void printNode(node_t* node);

void freeNodeRecursive(node_t** nodeAddr);

int pinNode(node_t* parent, node_t* child);

node_t* unpinNodeFromParent(node_t* node);

node_t* unpinLinkedNode(node_t* node);

int compareNode(node_t* cmp1, node_t* cmp2);

void resetLevelRecursive(node_t* startNode, int startLevel);

void resetLevelLinkedListRecursive(node_t* node, int startLevel);

void printTree(node_t* root); 

void printShortNode(node_t* node);

int pinLinkedListNode(node_t* parent, node_t* child);

void freeLinkedNodeRecursive(node_t* node);

void printLinkedNode(node_t* node);
